import React from 'react';
import { View } from 'react-native';
import LocationView from './views/Location';
import CategorySelection from './views/CategorySelection';
import SubCategorySelection from './views/SubCategorySelection';
import ItemDescriptionView from './views/ItemDescriptionView';
import * as Location from 'expo-location';
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
import Geocoder from 'react-native-geocoding';
import translations from './constants/translations';
import config from './constants/config';
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

Location.installWebGeolocationPolyfill();
i18n.translations = translations;
i18n.locale = Localization.locale;
i18n.fallbacks = true;
Geocoder.init(config.googleApiKey);

const Stack = createStackNavigator();
const defaultHeaderOptions = {
	title: i18n.t('common.title'),
	headerStyle: {
		backgroundColor: '#F5F1FE',
	},
	headerLeft: () => (
		<View style={{ marginLeft: 10 }}>
			<Ionicons name="md-menu" size={30} />
		</View>
	)
};

function AppNavigationStack() {
	return (
		<Stack.Navigator initialRouteName="Categories">
			<Stack.Screen name="Categories" component={CategorySelection} options={defaultHeaderOptions} />
			<Stack.Screen name="SubCategories" component={SubCategorySelection} options={defaultHeaderOptions} />
			<Stack.Screen name="DescriptionView" component={ItemDescriptionView} options={defaultHeaderOptions} />
			<Stack.Screen name="Map" component={LocationView} options={defaultHeaderOptions} />
		</Stack.Navigator>
	);
}

export default function App() {
	return (
		<NavigationContainer>
			<AppNavigationStack />
		</NavigationContainer>
	);
}
