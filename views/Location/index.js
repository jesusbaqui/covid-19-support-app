import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Alert } from 'react-native';
import { Button } from 'react-native-elements';

import * as Location from 'expo-location';
import MapView, { Marker } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import i18n from 'i18n-js';
import Geocoder from 'react-native-geocoding';
import config from '../../constants/config';

export default function view({ route, navigation }) {
    const [lang, setLang] = useState(null);
    const [address, setAddress] = useState("");
    const [location, setLocation] = useState(null);
    const [markerLatLng, setMarkerLatLng] = useState(null);

    const { item, type, description } = route.params;

    useEffect(() => {
        (async () => {
            if (location == null) {
                let { status } = await Location.requestPermissionsAsync();
                if (status !== 'granted')
                    return;

                let location = await Location.getCurrentPositionAsync({});
                setMarker(location.coords.latitude, location.coords.longitude, true)
                setLocation(location);
                setLang(i18n.locale.split('-')[0]);
            }
        })();
    });

    const setMarker = (lat, lng, center) => {
        setMarkerLatLng({
            latitude: lat,
            longitude: lng,
        });

        if (center && this.mapRef) {
            this.mapRef.animateToRegion({
                latitude: parseFloat(lat),
                longitude: parseFloat(lng),
                latitudeDelta: 0.0043,
                longitudeDelta: 0.0034
            });
        }
    }

    const setMarkerFromAddress = async (address) => {
        const geocodes = await Geocoder.from(address);
        if (!geocodes.results)
            return;

        let location = geocodes.results[0].geometry.location;
        setAddress(address);
        setMarker(location.lat, location.lng, true);
    }

    const setMarkerFromLocation = async (coordinate) => {
        const geocodes = await Geocoder.from(coordinate);
        if (!geocodes.results)
            return;

        let address = geocodes.results[0].formatted_address;
        let location = geocodes.results[0].geometry.location;
        setAddress(address);
        setMarker(location.lat, location.lng);
    }

    return (
        <View style={StyleSheet.absoluteFillObject}>
            <MapView
                ref={e => this.mapRef = e}
                style={StyleSheet.absoluteFillObject}
            >
                {
                    markerLatLng &&
                    <Marker
                        draggable
                        coordinate={markerLatLng}
                        onDragEnd={(e) => {
                            setMarkerFromLocation(e.nativeEvent.coordinate, true);
                        }}
                    />
                }
            </MapView>
            <GooglePlacesAutocomplete
                placeholder={i18n.t('map.searchPlaceholder')}
                returnKeyType={'search'}
                onPress={(data, details = null) => {
                    debugger;
                    setMarkerFromAddress(details.description || details.formatted_address, true);
                }}
                textInputProps={{
                    onChange: e => setAddress(e.nativeEvent.text),
                    value: address
                }}
                query={{
                    key: config.googleApiKey, // Personal Google Api Key with "Google Places API Web Service" enabled.
                    language: i18n.locale,
                    types: 'address'
                }}
                GooglePlacesDetailsQuery={{ fields: 'address' }}
                currentLocation={true}
                currentLocationLabel={i18n.t('map.currentLocation')}
                nearbyPlacesAPI='GoogleReverseGeocoding'
                enablePoweredByContainer={false}
                styles={{
                    textInputContainer: {
                        backgroundColor: 'rgba(0,0,0,0)',
                        borderTopWidth: 0,
                        borderBottomWidth: 0,
                        marginTop: 30
                    },
                    textInput: {
                        marginLeft: 15,
                        marginRight: 15,
                        height: 38,
                        color: '#5d5d5d',
                        fontSize: 16,
                    },
                    predefinedPlacesDescription: {
                        color: '#1faadb',
                    },
                    listView: {
                        marginLeft: 15,
                        marginRight: 15,
                        backgroundColor: '#fff'
                    }
                }}
            />
            <View style={styles.buttonsContainer}>
                <View style={styles.backButtonContainer}>
                    <Button
                        type="outline"
                        title={i18n.t('common.back')}
                        onPress={() => navigation.goBack()}
                        titleStyle={{
                            color: "#CB1F37"
                        }}
                        buttonStyle={{
                            backgroundColor: "#F5F1FE",
                            borderRadius: 12,
                            borderColor: "#CB1F37"
                        }}
                    >
                    </Button>
                </View>
                <View style={styles.continueButtonContainer}>
                    <Button
                        type="outline"
                        title={i18n.t(address ? 'common.next' : 'common.skip')}
                        onPress={() => {
                            Alert.alert(
                                i18n.t('map.success'),
                                i18n.t('map.succesMessage', { type, name: item.name[lang] || item.name.en }),
                                [{ text: "OK" }],
                                { cancelable: false }
                            );
                        }}
                        titleStyle={{
                            color: "white"
                        }}
                        buttonStyle={{
                            backgroundColor: "#CB1F37",
                            borderRadius: 12,
                            borderColor: "#CB1F37"
                        }}
                    />
                </View>
            </View>
            <View style={styles.footerContainer}>
                <Text style={styles.footerText}>{i18n.t('map.footerText')}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    footerContainer: {
        backgroundColor: '#F5F1FE',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        height: 90
    },
    buttonsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        height: 60
    },
    backButtonContainer: {
        width: 150,
        paddingRight: 10
    },
    continueButtonContainer: {
        width: 140,
    },
    footerText: {
        fontWeight: "bold"
    }
});