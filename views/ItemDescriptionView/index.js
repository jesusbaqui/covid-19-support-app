import React, { useState, useEffect } from 'react';
import i18n from 'i18n-js';

import { StyleSheet, View, Text, TextInput, I18nManager, Alert } from 'react-native';
import { Button } from 'react-native-elements';

export default function view({ route, navigation }) {
    const [lang, setLang] = useState(null);
    const [description, setDescription] = useState("");
    const [items, setItems] = useState([]);

    const { item } = route.params;

    useEffect(() => {
        (async () => {
            if (!lang) {
                let items = Object.values(item.subcategories);
                items = items.sort((a, b) => a.order - b.order);
                setItems(items);
                setLang(i18n.locale.split('-')[0]);
            }
        })();
    }, []);

    const nextStep = () => {
        let totalWords = description.split(" ").length;

        if (totalWords > 29) {
            Alert.alert("Error", i18n.t('descriptionView.inputLimitError', { totalWords }));
            return;
        }

        navigation.navigate("Map", { ...route.params, description });
    }

    return (
        <View style={StyleSheet.absoluteFillObject}>
            <View style={styles.titleContainer}>
                <Text style={{ fontSize: 22, fontWeight: 'bold' }}>
                    {item.name[lang] || item.name.en}
                </Text>
                <Text>
                    {i18n.t('descriptionView.subTitle')}
                </Text>
            </View>
            <View style={styles.descriptionContainer}>
                <TextInput
                    editable
                    multiline
                    placeholder={i18n.t('descriptionView.inputPlaceholder')}
                    numberOfLines={10}
                    onChangeText={text => setDescription(text)}
                    value={description}
                    maxLength={200}
                    style={styles.textInput}
                />
            </View>
            <View style={styles.buttonsContainer}>
                <View style={styles.backButtonContainer}>
                    <Button
                        type="outline"
                        title={i18n.t('common.back')}
                        titleStyle={{
                            color: "#CB1F37"
                        }}
                        buttonStyle={{
                            backgroundColor: "#F5F1FE",
                            borderRadius: 12,
                            borderColor: "#CB1F37"
                        }}
                        onPress={() => navigation.goBack()}
                    >
                    </Button>
                </View>
                <View style={styles.continueButtonContainer}>
                    <Button
                        type="outline"
                        title={i18n.t('common.next')}
                        titleStyle={{
                            color: "white"
                        }}
                        buttonStyle={{
                            backgroundColor: "#CB1F37",
                            borderRadius: 12,
                            borderColor: "#CB1F37"
                        }}
                        disabled={!description}
                        onPress={nextStep}
                    >
                    </Button>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    titleContainer: {
        padding: 30,
        backgroundColor: "#F5F1FE"
    },
    descriptionContainer: {
        flex: 3,
        padding: 30,
        backgroundColor: "#F5F1FE",
        alignItems: 'flex-start'
    },
    buttonsContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        height: 60,
        backgroundColor: "#F5F1FE"
    },
    backButtonContainer: {
        width: 150,
        paddingRight: 10
    },
    continueButtonContainer: {
        width: 140,
    },
    textInput: {
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        textAlignVertical: "top"
    }
});