import React, { useState, useEffect } from 'react';
import i18n from 'i18n-js';
import data from '../../constants/heyirys_covid.json';

import { StyleSheet, View, Text, Image, ScrollView } from 'react-native';
import { Button, ListItem } from 'react-native-elements';

export default function view({ navigation }) {
    const [lang, setLang] = useState(null);
    const [category, setCategory] = useState(null);
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        (async () => {
            if (!lang) {
                let categories = Object.values(data.categories);
                categories = categories.sort((a, b) => a.order - b.order);
                setCategories(categories);
                setLang(i18n.locale.split('-')[0]);
            }
        })();
    }, []);

    const getItemName = (item) => {
        return item.name[lang] || item.name.en;
    }

    const nextStep = () => {
        if (category.subcategories) {
            navigation.navigate("SubCategories", { item: category, requireDescription: category.requireDescription });
        }
        else if (category.requireDescription) {
            navigation.navigate("DescriptionView", { type: i18n.t("category.title"), item: category });
        }
        else {
            navigation.navigate("Map", { type: i18n.t("category.title"), item: category });
        }
    }

    return (
        <View style={StyleSheet.absoluteFillObject}>
            <View style={styles.titleContainer}>
                <Text style={{ fontSize: 30, fontWeight: 'bold' }}>{i18n.t('category.title')}</Text>
                <Text>{i18n.t('category.subTitle')}</Text>
            </View>
            <ScrollView style={styles.listContainer}>
                {
                    categories.map((i, index) => (
                        <ListItem
                            key={index}
                            leftIcon={<Image style={{ width: 50, height: 50 }} source={{ uri: i.icon }} />}
                            title={getItemName(i)}
                            titleStyle={{ fontWeight: 'bold' }}
                            containerStyle={{
                                backgroundColor: i == category ? "#1D0F30" : "#F5F1FE"
                            }}
                            titleStyle={{
                                color: i == category ? "white" : "black"
                            }}
                            onPress={() => setCategory(i)}
                            bottomDivider
                            chevron
                        />
                    ))
                }
            </ScrollView>
            <View style={styles.buttonsContainer}>
                <View style={styles.backButtonContainer}>
                    <Button
                        type="outline"
                        title={i18n.t('common.back')}
                        titleStyle={{
                            color: "#CB1F37"
                        }}
                        buttonStyle={{
                            backgroundColor: "#F5F1FE",
                            borderRadius: 12,
                            borderColor: "#CB1F37"
                        }}
                    >
                    </Button>
                </View>
                <View style={styles.continueButtonContainer}>
                    <Button
                        type="outline"
                        title={i18n.t('common.next')}
                        titleStyle={{
                            color: "white"
                        }}
                        buttonStyle={{
                            backgroundColor: "#CB1F37",
                            borderRadius: 12,
                            borderColor: "#CB1F37"
                        }}
                        disabled={category == null}
                        onPress={nextStep}
                    >
                    </Button>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    titleContainer: {
        padding: 30,
        backgroundColor: "#F5F1FE"
    },
    listContainer: {
        flex: 3,
        backgroundColor: "#F5F1FE"
    },
    buttonsContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        height: 60,
        backgroundColor: "#F5F1FE"
    },
    backButtonContainer: {
        width: 150,
        paddingRight: 10
    },
    continueButtonContainer: {
        width: 140,
    }
});