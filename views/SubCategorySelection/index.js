import React, { useState, useEffect } from 'react';
import i18n from 'i18n-js';

import { StyleSheet, View, Text, Image, ScrollView } from 'react-native';
import { ListItem } from 'react-native-elements';

export default function view({ route, navigation }) {
    const [lang, setLang] = useState(null);
    const [items, setItems] = useState([]);

    const { item, requireDescription } = route.params;

    useEffect(() => {
        (async () => {
            if (!lang) {
                let items = Object.values(item.subcategories);
                items = items.sort((a, b) => a.order - b.order);
                setItems(items);
                setLang(i18n.locale.split('-')[0]);
            }
        })();
    }, []);

    const getItemName = (item) => {
        return item.name[lang] || item.name.en;
    }

    const onItemSelection = (item) => {
        if (item.subcategories) {
            navigation.navigate("SubCategories", { item, requireDescription });
        }
        else if (requireDescription) {
            navigation.navigate("DescriptionView", { type: i18n.t("subCategory.title"), item });
        }
        else {
            navigation.navigate("Map", { type: i18n.t("subCategory.title"), item });
        }
    }

    return (
        <View style={StyleSheet.absoluteFillObject}>
            <View style={styles.titleContainer}>
                <Text style={{ fontSize: 22, fontWeight: 'bold' }}>
                    {item.name[lang] || item.name.en}
                </Text>
                {
                    item.question &&
                    <Text>
                        {item.question[lang] || item.question.en}
                    </Text>
                }
            </View>
            <ScrollView style={styles.listContainer}>
                {
                    items.map((i, index) => (
                        <ListItem
                            key={index}
                            leftIcon={<Image style={{ width: 50, height: 50 }} source={{ uri: i.icon }} />}
                            title={getItemName(i)}
                            titleStyle={{ fontWeight: 'bold' }}
                            onPress={() => onItemSelection(i)}
                            containerStyle={{
                                backgroundColor: "#F5F1FE"
                            }}
                            bottomDivider
                            chevron
                        />
                    ))
                }
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    titleContainer: {
        padding: 30,
        backgroundColor: "#F5F1FE"
    },
    listContainer: {
        flex: 1,
        backgroundColor: "#F5F1FE"
    },
    buttonsContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        height: 60,
        backgroundColor: "#F5F1FE"
    },
    backButtonContainer: {
        width: 150,
        paddingRight: 10
    },
    continueButtonContainer: {
        width: 140,
    }
});