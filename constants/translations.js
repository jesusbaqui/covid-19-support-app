export default {
    en: {
        permissions: {
            erros: {
                location: 'Permission to access location was denied',
            }
        },
        map: {
            searchPlaceholder: "Type your location",
            currentLocation: "Current location",
            footerText: "Please provide your approximate location. This information will be kept confidential",
            success: "Success",
            succesMessage: "Support request about {{type}} \"{{name}}\" submitted"
        },
        category: {
            title: "Category",
            subTitle: "Select an option to continue",
        },
        subCategory: {
            title: "SubCategory",
        },
        descriptionView: {
            inputPlaceholder: "Type here, 200 characters or 29 words allowed",
            subTitle: "Please provide a Brief Description of the issue.",
            inputLimitError: "The maximun words allowed (29) reached, you entered {{totalWords}}"
        },
        common: {
            back: "Back",
            skip: "Skip",
            next: "Next",
            title: "COVID-19 Support"
        }
    },
    es: {
        permissions: {
            erros: {
                location: 'El permiso para acceder a su posición fue denegado',
            }
        },
        map: {
            searchPlaceholder: "Escribe tu posición",
            currentLocation: "Posición actual",
            footerText: "Por favor proporcione su posición aproximada, esta información se mantendra confidencial.",
            success: "Correcto",
            succesMessage: "La peticion de soporte de la {{type}} \"{{name}}\" se ha enviado"
        },
        category: {
            title: "Categoria",
            subTitle: "Selecciona una opción para continuar",
        },
        subCategory: {
            title: "Subcategoria",
        },
        descriptionView: {
            inputPlaceholder: "Escribe aqui, 200 caracteres o 29 palabras permitidas",
            subTitle: "Por favor introduce una descripción breve.",
            inputLimitError: "Se alcanzó el límite máximo de palabras (29), ingresaste {{totalWords}}"
        },
        common: {
            back: "Regresar",
            skip: "Omitir",
            next: "Siguiente",
            title: "Soporte COVID-19",
        }
    },
};